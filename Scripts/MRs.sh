#!/bin/bash

# Check if jq is installed
if ! type "jq" > /dev/null; then
    echo please install jq
fi

title="$(git log --oneline | head -1 | cut -f 2- -d ' ')"
description="."

while [ "$#" -gt 0 ]; do
    case "$1" in
        --source-branch)
            source_branch="$2"
            shift
            shift
            ;;
        --target-branch)
            target_branch="$2"
            shift
            shift
            ;;
        --title)
            title="$2"
            shift
            shift
            ;;
        --description)
            description="$2"
            shift
            shift
            ;;
        -h)
            echo "Usage:"
            echo "  $ git merge-request [OPTIONS]"
            echo ""
            echo "Options:"
            echo "  --source-branch    Source branch of the merge request. (default: current branch)"
            echo "  --target-branch    Target branch of the merge request. (default: master)"
            echo "  --title            Title of the merge request. (default: first line of last commit log)"
            echo "  --description      Description of the merge request. (default: empty)"
            echo ""
            exit 0
            ;;
    esac
done


HOST="$(git config --get gitlab.url)"
if [ "$HOST" == "" ]; then
    echo "Gitlab url is not set"
    echo "  $ git config --replace-all gitlab.url '$HOST'"
    exit 1
fi

CI_PROJECT_ID="$(git config --local --get gitlab.id)"
if [ "$CI_PROJECT_ID" == "" ] || [ "$project_id" -eq 1 ]; then
    echo "Project id is not set, you will find it in gitlab project settings"
    echo "  $ git config --local --replace-all gitlab.id '$CI_PROJECT_ID'"
    exit 1
fi

repo="$(git config --local --get gitlab.repo)"
if [ "$repo" == "" ]; then
    echo "Repository name is not set"
    echo "  $ git config --local --replace-all gitlab.repo 'cartter01/test"
    exit 1
fi

private_token="$(git config --get gitlab.privatetoken)"
if [ "$private_token" == "" ]; then
    echo "Private token is not set"
    echo "  $ git config --local --add gitlab.privatetoken '$ACCESS_TOKEN'"
    exit 1
fi

remote="$(git config --get gitlab.remote)"
if [ "$remote" == "" ]; then
    remote=origin
fi

if ! git push -u ${remote} ${source_branch}; then
    echo "Failed to push ${target_branch}"
    exit 1
fi

merge_request_id=$(curl \
  -vv \
  --insecure \
  -s \
  --header "PRIVATE-TOKEN: $private_token" \
  -d source_branch="${source_branch}" \
  -d target_branch="${target_branch}" \
  -d title="${title}" \
  "${gitlab_url}/api/v3/projects/${project_id}/merge_requests" \
    | jq '.iid')

echo "Open ${gitlab_url}/${repo}/merge_requests/${merge_request_id}"
